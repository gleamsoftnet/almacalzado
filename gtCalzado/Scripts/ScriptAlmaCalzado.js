﻿
$(document).ready(function () {
    var parameter;
    $('#buscador').keypress(function (e) {
        if (e.which === 13) {
            e.preventDefault();
            parameter = $("#buscador").val();
            GetData();
        }
    });
    $(".add-row").click(function () {
        parameter = $("#buscador").val();
        GetData();
    });
    function GetData() {
        $("#buscador").val("");
        $.ajax({
            url: "/Home/GetProduct",
            type: "POST",
            dataType: "json",
            data: { Prefix: parameter },
            success: function (data) {
                $.each(data, function (index, itemData) {
                    if (itemData.Cantidad > 0) {
                        var markup = "<tr><td style='text-align:center;'>"
                            + "<input type='checkbox' name='record'>"
                            + "</td><td class='ProdId'>"
                            + itemData.Id + "</td><td>"
                            + itemData.Referencia + "</td><td>"
                            + itemData.Nombre + "</td><td>"
                            + itemData.Proveedor + "</td><td>"
                            + itemData.Color + "</td><td>"
                            + itemData.Numero + "</td><td class='pd-price'>"
                            + formatMoney(itemData.PrecioVenta, '$')
                            + "</td><td style='text-align:center;'>1</td>"
                            + "<td style='text-align:center;'>" + itemData.Cantidad + "</td></tr>";
                        $("table tbody").append(markup);
                        //call total bill
                        totalBill();
                    } else {
                        alert("Producto agotado");
                    }
                });
            }
        });
    };

    //total bill
    function totalBill() {
        var total = 0;

        $("#myTable tr").each(function () {
            var currentRow = $(this);
            var priceRow = currentRow.find(".pd-price").html();
            if (typeof (priceRow) !== "undefined") {
                total += parseFloat(priceRow.substr(1));
            }

        });
        var html = "<strong>" + formatMoneyTotal(total, '$') + "<br>N / A <br>" + formatMoneyTotal(total, '$') + "</strong> ";
        $("#totalBill").empty();
        $("#totalBill").append(html);
    };

    //format money
    function formatMoney(n, currency) {
        return currency + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    };
    function formatMoneyTotal(n, currency) {
        return currency + n.toFixed(3).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    };
    // Find and remove selected table rows
    $(".delete-row").click(function () {
        $("table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
                totalBill();
            }
        });
    });
    $("#opciones").click(function () {
        showOptions();
    });
    function showOptions() {
        var x = document.getElementById("mostrarOpcion");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    $("#buscador").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Home/Index",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {

                        return {
                            label: item.Nombre + ' - ' + item.Referencia,
                            value: item.Referencia
                        };

                    }));

                }
            });
        },
        messages: {
            noResults: "No existe", function(resultsCount) {

            }
        }

    });
    //cargar cantidades por docenas
    $("#Decenas").change(function () {
        var SID = $(this).val();
        $.getJSON("../Comprar/LoadCantidades", { docenasId: SID },
            function (data) {
                $("#ProductosList").empty();

                $.each(data, function (index, itemData) {

                    $("#ProductosList").append('<option value="'
                        + itemData.Id + '">' +
                        itemData.Numero +
                        " ( " + itemData.Cantidad +
                        " )" + '</option>');
                });
                $('#ProductosList option').prop('selected', true);
            });
    });
    //cargar productos por proveedor
    $("#ProveedorId").change(function () {
        var SID = $(this).val();
        $.getJSON("../Comprar/LoadProductos", { proveedorId: SID },
            function (data) {

                $("#ZapatoId").empty();
                $.each(data, function (index, itemData) {
                    $("#ZapatoId").append('<option value="'
                        + itemData.Id + '">' +
                        itemData.Nombre + '</option>');
                });

            });
    });
    //crear cliente
    $("#btncrearcliente").click(function () {
        var cedula = $("#Cedula").val();
        var nombre = $("#Nombre").val();
        var telefono = $("#Telefono").val();
        var objCliente = {
            "Cedula": cedula, "Nombre": nombre, "Telefono": telefono
        };
        $.ajax({
            url: '/Home/NewCliente',
            type: "POST",
            data: JSON.stringify(objCliente),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: function (response) {
                alert(response.responseText);
            },
            success: function (response) {
                alert("Cliente creado correctamente!");
                $("#txtIdCliente").val(response.Id);
                $("#txtCedulaCliente").val(response.Cedula);
                $("#txtNombreCliente").val(response.Nombre);
                $("#txtTelefono").val(response.Telefono);
                $('#myModal').modal('hide');

            }
        });

    });

    //Obtener datos cliente

    $('#txtCedulaCliente').keypress(function (e) {
        if (e.which === 13) {
            e.preventDefault();
            var parameter = $("#txtCedulaCliente").val();
            GetDataCliente(parameter);
        }
    });
    function GetDataCliente(request) {
        $.ajax({
            url: "/Home/GetDatosCliente",
            type: "POST",
            dataType: "json",
            data: { Prefix: request },
            success: function (data) {
                if (data.length > 0) {
                    $.map(data, function (item) {
                        $("#txtIdCliente").val(item.Id);
                        $("#txtNombreCliente").val(item.Nombre);
                        $("#txtTelefono").val(item.Telefono);
                        return {
                            label: item.Nombre + ' - ' + item.Cedula,
                            value: item.Cedula
                        };


                    });
                } else {
                    alert("Sin registro para esta cédula");
                }

            }
        });
    }

    //enviar datos formulario de ventas
    var inputCountNull = 0;
    var inputTextNull = "";
    $("#btnGuardarVentaEfectivo").click(function () {
        validateInpunt();
        if (inputCountNull > 0) {
            $("#errorAlert").html(inputTextNull);
            inputCountNull = 0;
        } else {
            guardarPedido("Efectivo");
        }
    });
    $("#btnGuardarVentaTarjeta").click(function () {
        validateInpunt();
        if (inputCountNull > 0) {
            $("#errorAlert").html(inputTextNull);
             
        } else {
            guardarPedido("Tarjeta");
        }
    });
    $("#btnGuardarVentaCodensa").click(function () {
        validateInpunt();
        if (inputCountNull > 0) {
            $("#errorAlert").html(inputTextNull);
             
        } else {
            guardarPedido("Codensa");
        }
    });
    $("#btnGuardarVentaSodexo").click(function () {
        validateInpunt();
        if (inputCountNull > 0) {
            $("#errorAlert").html(inputTextNull);

        } else {
            guardarPedido("Sodexo");
        }
    });
    $("#btnGuardarVentaBigPass").click(function () {
        validateInpunt();
        if (inputCountNull > 0) {
            $("#errorAlert").html(inputTextNull);
            
        } else {
            guardarPedido("BigPass");
        }
    });
    function validateInpunt() {
        inputCountNull = 0;
        inputTextNull = "";
        var txtIdVendedor = $('#txtIdVendedor').val();
        var txtCedulaCliente = $('#txtCedulaCliente').val();
        var txtIdCliente = $('#txtIdCliente').val();
        if (txtIdVendedor.length < 1) {
            inputTextNull += "Código vendedor requerido, ";
            inputCountNull++;
        }
        if (txtCedulaCliente.length < 1) {
            inputTextNull += "Cédula requerida, ";
            inputCountNull++;
        }
        if (txtIdCliente.length < 1) {
            inputTextNull += "Id cliente no encontrado,";
            inputCountNull++;
        }
    };
    function guardarPedido(param) {
        
        var jsonObj = [];
        var jsonResult = {};
        $("#myTable tr").each(function (index, value) {
            var currentRow = $(this);
            var id = currentRow.find(".ProdId").html();
            if (typeof (id) !== "undefined") {
                var lang_array = {};
                lang_array["ProductoId"] = parseInt(id);
                jsonObj.push(lang_array);
            }
        });
        var txtIdCliente = $('#txtIdCliente').val();
        var txtIdVendedor = $('#txtIdVendedor').val();
        var txtDescuento = $('#txtDescuento').val();
        jsonResult["ClienteId"] = parseInt(txtIdCliente);
        jsonResult["Descuento"] = parseInt(txtDescuento);
        jsonResult["AsesorId"] = parseInt(txtIdVendedor);
        jsonResult["MetodoPago"] = param;
        jsonResult["ProductoVenta"] = jsonObj;

        //send data

        $.ajax({
            url: "../Home/AddVenta",
            type: "POST",
            data: JSON.stringify(jsonResult),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: function (response) {
                alert(response.responseText);
                $("#myTable > tr").remove();
            },
            success: function (response) {
                alert(response.Response);
                $("#myTable > tr").remove();
                GetVentas();
            }
        });
    };
    //obtener las ultimas 10 ventas
    function GetVentas() {

    }

});
