﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(gtCalzado.Startup))]
namespace gtCalzado
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
