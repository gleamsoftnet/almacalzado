﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace gtCalzado.Controllers
{
    public class FacturasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Facturas
        public ActionResult Index()
        {
            ViewBag.RefCompraId = new SelectList(db.RefCompras, "Id", "Nombre");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection formCollection)
        {
            ViewBag.RefCompraId = new SelectList(db.RefCompras, "Id", "Nombre");
            if (formCollection["RefCompraId"] != null)
            {
                int refId = Int32.Parse(formCollection["RefCompraId"]);
                 
                return View(db.Compras.Where(u => u.RefCompraId == refId).ToList());

            }             
            return View();
        }
    }
}