﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using Microsoft.AspNet.Identity;
using gtCalzado.Util;

namespace gtCalzado.Controllers
{
    [Authorize(Roles ="Empresa")]
    public class ProveedoresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Proveedores
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            return View(await db.Proveedor.Where(p=>p.UserId==user).ToListAsync());
        }

        // GET: Proveedores/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proveedor proveedor = await db.Proveedor.FindAsync(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        // GET: Proveedores/Create
        public ActionResult Create()
        {
            var user = User.Identity.GetUserId();
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(s=>s.UserId==user), "Id", "Nombre");
            return View();
        }

        // POST: Proveedores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,SucursalId,Codigo,Cedula,Nombre,Telefono,Direccion,Estado,FechaRegistro")] Proveedor proveedor)
        {
            var user = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
               
                proveedor.UserId = user;
                proveedor.Estado = true;
                proveedor.FechaRegistro = DateTimeOffset.Now;
                db.Proveedor.Add(proveedor);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(s => s.UserId == user), "Id", "Nombre");
            return View(proveedor);
        }

        // GET: Proveedores/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proveedor proveedor = await db.Proveedor.FindAsync(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            var user = User.Identity.GetUserId();
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(s => s.UserId == user), "Id", "Nombre", proveedor.SucursalId);
            return View(proveedor);
        }

        // POST: Proveedores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,SucursalId,Codigo,Cedula,Nombre,Telefono,Direccion,Estado,FechaRegistro")] Proveedor proveedor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proveedor).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var user = User.Identity.GetUserId();
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(s => s.UserId == user), "Id", "Nombre", proveedor.SucursalId);
            return View(proveedor);
        }

        // GET: Proveedores/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proveedor proveedor = await db.Proveedor.FindAsync(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        // POST: Proveedores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Proveedor proveedor = await db.Proveedor.FindAsync(id);
            db.Proveedor.Remove(proveedor);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
