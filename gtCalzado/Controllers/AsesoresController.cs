﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using Microsoft.AspNet.Identity;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class AsesoresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Asesores
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            return View(await db.Asesores.Where(u=>u.EmpresaId==user).ToListAsync());
        }

        // GET: Asesores/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asesores asesores = await db.Asesores.FindAsync(id);
            if (asesores == null)
            {
                return HttpNotFound();
            }
            return View(asesores);
        }

        // GET: Asesores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Asesores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Codigo,Nombre,FechaRegistro,EmpresaId")] Asesores asesores)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                asesores.EmpresaId = user;
                db.Asesores.Add(asesores);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(asesores);
        }

        // GET: Asesores/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asesores asesores = await db.Asesores.FindAsync(id);
            if (asesores == null)
            {
                return HttpNotFound();
            }
            return View(asesores);
        }

        // POST: Asesores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Codigo,Nombre,FechaRegistro,EmpresaId")] Asesores asesores)
        {
            if (ModelState.IsValid)
            {
                db.Entry(asesores).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(asesores);
        }

        // GET: Asesores/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asesores asesores = await db.Asesores.FindAsync(id);
            if (asesores == null)
            {
                return HttpNotFound();
            }
            return View(asesores);
        }

        // POST: Asesores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Asesores asesores = await db.Asesores.FindAsync(id);
            db.Asesores.Remove(asesores);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
