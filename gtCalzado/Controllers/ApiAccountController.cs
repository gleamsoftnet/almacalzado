﻿using gtCalzado.Models;
using gtCalzado.Service;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace gtCalzado.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class ApiAccountController : ApiController
    {
        private SVAuthentication svAuthentications = new SVAuthentication();
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationDbContext db;
        
        private SVUsers sVUsers;
        public ApiAccountController()
        {
            sVUsers = new SVUsers();
             
            db = new ApplicationDbContext();
        }
        public ApiAccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;

            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return null;
                //return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [OverrideAuthentication]
        [AllowAnonymous]
        [Route("Login")]
        public UserInfoViewModel ValidateUser(LoginViewModelApi model)
        {
            var result = SignInManager.PasswordSignIn(model.Usuario, model.Contrasena, false, false);
            if (result.Equals(SignInStatus.Success))
            {
                UserInfoViewModel resp = svAuthentications.GetDataUserId(model.Usuario);
                //ApplicationUser currentUser = sVUsers.GetUserData(model.Usuario);
                  
                return resp;
            }
            else
            {
                return null;

            }

        }
    }
}
