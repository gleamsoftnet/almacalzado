﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;

namespace gtCalzado.Controllers
{
    public class DevolucionVentaLibresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DevolucionVentaLibres
        public async Task<ActionResult> Index()
        {
            var devolucionVentaLibre = db.DevolucionVentaLibre.Include(d => d.VentaLibre);
            return View(await devolucionVentaLibre.ToListAsync());
        }

        // GET: DevolucionVentaLibres/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DevolucionVentaLibre devolucionVentaLibre = await db.DevolucionVentaLibre.FindAsync(id);
            if (devolucionVentaLibre == null)
            {
                return HttpNotFound();
            }
            return View(devolucionVentaLibre);
        }

        // GET: DevolucionVentaLibres/Create
        public ActionResult Create()
        {
            ViewBag.VentaLibreId = new SelectList(db.VentaLibre, "Id", "Factura");
            return View();
        }

        // POST: DevolucionVentaLibres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Diferencia,UsuarioId,Fecha,VentaLibreId")] DevolucionVentaLibre devolucionVentaLibre)
        {
            if (ModelState.IsValid)
            {
                db.DevolucionVentaLibre.Add(devolucionVentaLibre);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.VentaLibreId = new SelectList(db.VentaLibre, "Id", "Factura", devolucionVentaLibre.VentaLibreId);
            return View(devolucionVentaLibre);
        }

        // GET: DevolucionVentaLibres/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DevolucionVentaLibre devolucionVentaLibre = await db.DevolucionVentaLibre.FindAsync(id);
            if (devolucionVentaLibre == null)
            {
                return HttpNotFound();
            }
            ViewBag.VentaLibreId = new SelectList(db.VentaLibre, "Id", "Factura", devolucionVentaLibre.VentaLibreId);
            return View(devolucionVentaLibre);
        }

        // POST: DevolucionVentaLibres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Diferencia,UsuarioId,Fecha,VentaLibreId")] DevolucionVentaLibre devolucionVentaLibre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(devolucionVentaLibre).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.VentaLibreId = new SelectList(db.VentaLibre, "Id", "Factura", devolucionVentaLibre.VentaLibreId);
            return View(devolucionVentaLibre);
        }

        // GET: DevolucionVentaLibres/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DevolucionVentaLibre devolucionVentaLibre = await db.DevolucionVentaLibre.FindAsync(id);
            if (devolucionVentaLibre == null)
            {
                return HttpNotFound();
            }
            return View(devolucionVentaLibre);
        }

        // POST: DevolucionVentaLibres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DevolucionVentaLibre devolucionVentaLibre = await db.DevolucionVentaLibre.FindAsync(id);
            db.DevolucionVentaLibre.Remove(devolucionVentaLibre);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
