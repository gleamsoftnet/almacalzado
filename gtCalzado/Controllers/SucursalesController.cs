﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using Microsoft.AspNet.Identity;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class SucursalesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Sucursales
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            return View(await db.Sucursales.Where(s=>s.UserId==user).ToListAsync());
        }

        // GET: Sucursales/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sucursales sucursales = await db.Sucursales.FindAsync(id);
            if (sucursales == null)
            {
                return HttpNotFound();
            }
            return View(sucursales);
        }

        // GET: Sucursales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sucursales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Nombre,Direccion,Estado,FechaRegistro")] Sucursales sucursales)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                sucursales.UserId = user;
                sucursales.FechaRegistro = DateTimeOffset.Now;
                sucursales.Estado = true;
               db.Sucursales.Add(sucursales);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sucursales);
        }

        // GET: Sucursales/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sucursales sucursales = await db.Sucursales.FindAsync(id);
            if (sucursales == null)
            {
                return HttpNotFound();
            }
            return View(sucursales);
        }

        // POST: Sucursales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nombre,Direccion,Estado,FechaRegistro,UserId")] Sucursales sucursales)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sucursales).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sucursales);
        }

        // GET: Sucursales/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sucursales sucursales = await db.Sucursales.FindAsync(id);
            if (sucursales == null)
            {
                return HttpNotFound();
            }
            return View(sucursales);
        }

        // POST: Sucursales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Sucursales sucursales = await db.Sucursales.FindAsync(id);
            db.Sucursales.Remove(sucursales);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
