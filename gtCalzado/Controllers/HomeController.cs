﻿using gtCalzado.Models;
using gtCalzado.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private SVVentas sVVentas = new SVVentas();
        private SVZapato sVZapato = new SVZapato(); 
        private ApplicationDbContext db = new ApplicationDbContext();
        public async Task<ActionResult> Index()
        {
            //var user = User.Identity.GetUserId();
            //ViewBag.Proveedores = db.Proveedor.Where(u => u.UserId == user).ToList();
            //ViewBag.Categorias = db.Categorias.Where(u => u.UserId == user).ToList();
            //ViewBag.Tallas = db.Tallas.Where(u => u.UserId == user).ToList();
            //ViewBag.Colores = db.Colores.Where(u => u.UserId == user).ToList();
            return View();
        }
        [HttpPost]
        public JsonResult Index(string Prefix)
        {
            var user = User.Identity.GetUserId();
            //Searching records from list using LINQ query  
            var listZapatos = (from N in db.Zapatos
                               where N.Referencia.Contains(Prefix) && N.UserId == user
                               select new { N.Id, N.Nombre, N.Referencia });


            return Json(listZapatos, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetProduct(string Prefix)
        {
            var user = User.Identity.GetUserId();
            //Searching records from list using LINQ query  
            var listZapatos = (from N in db.Zapatos
                               where N.Referencia.Contains(Prefix) && N.UserId == user
                               select new
                               {
                                   N.Id,
                                   N.Nombre,
                                   N.Referencia,
                                   N.Cantidad,
                                   N.PrecioVenta,
                                   N.Talla.Numero,
                                   Color = N.Color.Nombre,
                                   Proveedor = N.Proveedor.Nombre
                               });


            return Json(listZapatos, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult NewCliente(Clientes model)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                model.FechaRegistro = DateTimeOffset.Now;
                model.Autor = user;
                db.Clientes.Add(model);
                db.SaveChanges();
                return Json(model);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddVenta(VenderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                var venta = new Ventas();
                venta.Referencia = Guid.NewGuid();
                venta.Cantidad = 1;
                venta.ClienteId = model.ClienteId;
                venta.Descuento = 0;
                venta.Estado = true;
                venta.FechaRegistro = DateTimeOffset.Now;
                venta.VendedorId = user;
                venta.MetodoPago = model.MetodoPago;
                venta.Descuento = model.Descuento;
                int asesorId = sVZapato.getAsesorIdFromCode(model.AsesorId);
                if ( asesorId> 0) {
                    venta.AsesorId = asesorId;
                }else
                {
                    asesorId=sVZapato.NuevoAsesor(model.AsesorId, user);
                    venta.AsesorId = asesorId;
                }                
                foreach (var item in model.ProductoVenta)
                {
                    venta.ZapatoId = item.ProductoId;
                    sVVentas.NuevaVenta(venta);
                    //actualizar stok
                    sVZapato.UpdateStockVenta(item.ProductoId, 1);
                }                
                return Json(new { Response = "Venta registrada correctamente",Referencia=venta.Referencia });
            }

            return Json(new { Response="Error! algo salio mal, intenta nuevamente",Referencia=""});
        }
        [HttpPost]
        public JsonResult GetDatosCliente(int Prefix)
        {
            var user = User.Identity.GetUserId();
            //Searching records from list using LINQ query  
            var listZapatos = (from N in db.Clientes
                               where N.Cedula == Prefix && N.Autor == user
                               select new { N.Id, N.Nombre, N.Cedula, N.Telefono });

            return Json(listZapatos, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDatosVentas()
        {
            var user = User.Identity.GetUserId();
             
            var listZapatos = (from N in db.Ventas
                               where  N.VendedorId == user
                               select new { N.Id, N.Referencia, N.Cantidad, N.MetodoPago });

            return Json(listZapatos, JsonRequestBehavior.AllowGet);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}