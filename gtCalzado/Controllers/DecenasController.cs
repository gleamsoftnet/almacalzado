﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using Microsoft.AspNet.Identity;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class DecenasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Decenas
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            return View(await db.Decenas.Where(u=>u.UserId==user).ToListAsync());
        }

        // GET: Decenas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Decenas decenas = await db.Decenas.FindAsync(id);
            if (decenas == null)
            {
                return HttpNotFound();
            }
            return View(decenas);
        }

        // GET: Decenas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Decenas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Nombre")] Decenas decenas)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                decenas.UserId = user;
                db.Decenas.Add(decenas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(decenas);
        }

        // GET: Decenas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Decenas decenas = await db.Decenas.FindAsync(id);
            if (decenas == null)
            {
                return HttpNotFound();
            }
            return View(decenas);
        }

        // POST: Decenas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nombre")] Decenas decenas)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                decenas.UserId = user;
                db.Entry(decenas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(decenas);
        }

        // GET: Decenas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Decenas decenas = await db.Decenas.FindAsync(id);
            if (decenas == null)
            {
                return HttpNotFound();
            }
            return View(decenas);
        }

        // POST: Decenas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Decenas decenas = await db.Decenas.FindAsync(id);
            db.Decenas.Remove(decenas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
