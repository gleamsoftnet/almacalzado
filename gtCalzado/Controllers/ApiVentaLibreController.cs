﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using gtCalzado.Models;

namespace gtCalzado.Controllers
{
    public class ApiVentaLibreController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ApiVentaLibre
        public IQueryable<VentaLibre> GetVentaLibre()
        {   
            var dt = DateTime.Today; //fecha actual
            return db.VentaLibre.Where(x => x.Fecha.Year == dt.Year
                            && x.Fecha.Month == dt.Month
                            && x.Fecha.Day == dt.Day);
        }
        
        // GET: api/ApiVentaLibre?factura=###
        public IQueryable<VentaLibre> GetVentaLibre(string factura)
        {
            return db.VentaLibre.Where(x=>x.Factura==factura);
        }

        // GET: api/ApiVentaLibre/5
        [ResponseType(typeof(VentaLibre))]
        public async Task<IHttpActionResult> GetVentaLibre(int id)
        {
            VentaLibre ventaLibre = await db.VentaLibre.FindAsync(id);
            if (ventaLibre == null)
            {
                return NotFound();
            }

            return Ok(ventaLibre);
        }

        // PUT: api/ApiVentaLibre/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVentaLibre(int id, VentaLibre ventaLibre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ventaLibre.Id)
            {
                return BadRequest();
            }

            db.Entry(ventaLibre).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VentaLibreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiVentaLibre
        [ResponseType(typeof(VentaLibre))]
        public async Task<IHttpActionResult> PostVentaLibre(VentaLibre ventaLibre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VentaLibre.Add(ventaLibre);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = ventaLibre.Id }, ventaLibre);
        }

        // DELETE: api/ApiVentaLibre/5
        [ResponseType(typeof(VentaLibre))]
        public async Task<IHttpActionResult> DeleteVentaLibre(int id)
        {
            VentaLibre ventaLibre = await db.VentaLibre.FindAsync(id);
            if (ventaLibre == null)
            {
                return NotFound();
            }

            db.VentaLibre.Remove(ventaLibre);
            await db.SaveChangesAsync();

            return Ok(ventaLibre);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VentaLibreExists(int id)
        {
            return db.VentaLibre.Count(e => e.Id == id) > 0;
        }
    }
}