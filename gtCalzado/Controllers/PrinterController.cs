﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using gtCalzado.Models;
using gtCalzado.Service;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static gtCalzado.Models.ManageProductosViewModel;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class PrinterController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private SVPrinter sVPrinter = new SVPrinter();
        private SVCompra sVCompra = new SVCompra();
        // GET: Printer
        public ActionResult Index()
        {
            var user = User.Identity.GetUserId();
            ViewBag.RefCompraId = new SelectList(db.RefCompras.Where(u => u.UserId == user), "Id", "Nombre");
              
            return View();
        }
        [HttpPost]
        public ActionResult Index(int RefCompraId)
        {
            if (RefCompraId > 0)
            {
                
                var query = sVCompra.GetCodigosBarras(RefCompraId);
                if (query != null)
                { 
                    ViewBag.ListCode = query;
                     
                }
            }

            var user = User.Identity.GetUserId();
            ViewBag.RefCompraId = new SelectList(db.RefCompras.Where(u => u.UserId == user), "Id", "Nombre");
            return View();
        }

        public ActionResult BarCodeGenerate(int RefCompraId)
        {
            Document doc = new Document(new iTextSharp.text.Rectangle(24, 12), 5, 5, 1, 1);
            DateTime dTime = DateTime.Now;
            string strPDFFileName = string.Format("CodigosProductos_" + dTime.ToString("yyyyMMddhhmm") + "_aczdo" + ".pdf");
            string path = Server.MapPath("~/CodigosBarras/" + strPDFFileName);
            try
            { 
                //PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(
                //  Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/"+strPDFFileName, FileMode.Create));
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                doc.Open();
                var provedorNombre = "";                
                System.Drawing.Image img1 = null;
                var codigosBarras = sVCompra.GetCodigosBarras(RefCompraId);
                foreach (var item in codigosBarras)
                {
                     
                    for (int i = 0; i < item.Cantidad; i++)
                    {
                        if (item != null)
                            doc.NewPage();
                        provedorNombre = item.Proveedor;

                        PdfContentByte cb1 = writer.DirectContent;
                        BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_BOLDITALIC, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                        cb1.SetFontAndSize(bf, 2.0f);
                        cb1.BeginText();
                        cb1.SetTextMatrix(1.2f, 9.5f);
                        cb1.ShowText(item.Nombre.ToString());
                        cb1.EndText();

                        PdfContentByte cb2 = writer.DirectContent;
                        BaseFont bf1 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cb2.SetFontAndSize(bf1, 1.3f);
                        cb2.BeginText();
                        cb2.SetTextMatrix(15.5f, 1.0f);
                        cb2.ShowText("$" + item.Precio.ToString());
                        cb2.EndText();

                        iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
                        iTextSharp.text.pdf.Barcode128 bc = new Barcode128();
                        bc.TextAlignment = Element.ALIGN_LEFT;
                        //agrego el el id producto para leerlo luego con la pistola y filtrar en la base de datos
                        bc.Code = item.Referencia;
                        bc.StartStopText = false;
                        bc.CodeType = iTextSharp.text.pdf.Barcode128.EAN13;
                        bc.Extended = true;

                        //System.Drawing.Image bimg = 
                        //  bc.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White);
                        //img1 = bimg;

                        iTextSharp.text.Image img = bc.CreateImageWithBarcode(cb,
                          iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);

                        cb.SetTextMatrix(1.5f, 3.0f);
                        img.ScaleToFit(60, 5);
                        img.SetAbsolutePosition(1.5f, 1);
                        cb.AddImage(img);
                    }
                }

                ////////////////////***********************************//////////////////////

                doc.Close();
                string domainName = Request.Url.GetLeftPart(UriPartial.Authority);
                var folderRoute = "/CodigosBarras/" + strPDFFileName;
                var fileRoute = domainName + folderRoute;
                //guardar documento
                var user = User.Identity.GetUserId();
                var barCode = new BarCode();
                barCode.Proveedor = provedorNombre;
                barCode.Referencia = RefCompraId;
                barCode.Ruta = folderRoute;
                barCode.Fecha = DateTimeOffset.Now;
                barCode.UserId = user;
                sVCompra.NuevaRutaBarCode(barCode);
                
                return Redirect(fileRoute);
            }
            catch
            {
            }
            finally
            {
                doc.Close();
            }
            return View();
            
        }

        public FileResult CreatePdf()
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            //file name to be created 
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            Document doc = new Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            //Create PDF Table with 5 columns
            PdfPTable tableLayout = new PdfPTable(5);
            doc.SetMargins(0f, 0f, 0f, 0f);
            //Create PDF Table

            //file will created in this path
            string strAttachment = Server.MapPath("~/Downloadss/" + strPDFFileName);


            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            //Add Content to PDF 
            doc.Add(Add_Content_To_PDF(tableLayout));

            // Closing the document
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;


            return File(workStream, "application/pdf", strPDFFileName);

        }


        protected PdfPTable Add_Content_To_PDF(PdfPTable tableLayout)
        {

            float[] headers = { 50, 24, 45, 35, 50 };  //Header Widths
            tableLayout.SetWidths(headers);        //Set the pdf headers
            tableLayout.WidthPercentage = 100;       //Set the PDF File witdh percentage
            tableLayout.HeaderRows = 1;
            //Add Title to the PDF file at the top

            List<Compra> employees = db.Compras.ToList();



            tableLayout.AddCell(new PdfPCell(new Phrase("Creating Pdf using ItextSharp", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0)))) { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            ////Add header
            AddCellToHeader(tableLayout, "Proveedor");
            AddCellToHeader(tableLayout, "Producto");
            AddCellToHeader(tableLayout, "Gender");
            AddCellToHeader(tableLayout, "City");
            AddCellToHeader(tableLayout, "Hire Date");

            ////Add body




            foreach (var emp in employees)
            {

                AddCellToBody(tableLayout, emp.Proveedor.Nombre);
                AddCellToBody(tableLayout, emp.RefCompra.Referencia);
                AddCellToBody(tableLayout, emp.RefCompra.Nombre);
                AddCellToBody(tableLayout, emp.Talla.Numero.ToString());
                AddCellToBody(tableLayout, emp.Color.Nombre.ToString());

            }

            return tableLayout;
        }

        // Method to add single cell to the Header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {

            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.YELLOW))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(128, 0, 0) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255) });
        }


    }
}