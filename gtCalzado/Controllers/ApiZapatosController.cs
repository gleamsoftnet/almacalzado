﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using gtCalzado.Models;
namespace gtCalzado.Controllers
{
    public class ApiZapatosController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET api/<controller>
        public List<ZapatoViewModel> GetZapatoRef(string referencia)
        {
            return (from z in db.Zapatos
                    join t in db.Tallas on z.TallaId equals t.Id
                    join c in db.Categorias on z.CategoriaId equals c.Id
                    join cl in db.Colores on z.ColorId equals cl.Id
                    join p in db.Proveedor on z.ProveedorId equals p.Id
                    where z.Referencia == referencia

                    select new ZapatoViewModel
                    {
                        Id = z.Id,
                        Nombre = z.Nombre,
                        Referencia = z.Referencia,
                        FechaRegistro = z.FechaRegistro,
                        Precio = z.Precio,
                        PrecioVenta = z.PrecioVenta,
                        Cantidad = z.Cantidad,
                        Proveedor = p.Nombre,
                        Color = cl.Codigo,
                        Talla = t.Codigo,
                        Categoria = c.Codigo,


                    }).ToList();

        }

    }
}