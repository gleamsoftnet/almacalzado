﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using Microsoft.AspNet.Identity;

namespace gtCalzado.Controllers
{
    public class CantidadesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Cantidades
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            var cantidades = db.Cantidades.Include(c => c.Decenas).Where(u=>u.Decenas.UserId==user);
            return View(await cantidades.ToListAsync());
        }

        // GET: Cantidades/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cantidades cantidades = await db.Cantidades.FindAsync(id);
            if (cantidades == null)
            {
                return HttpNotFound();
            }
            return View(cantidades);
        }

        // GET: Cantidades/Create
        public ActionResult Create()
        {
            var user = User.Identity.GetUserId();
            ViewBag.DecenasId = new SelectList(db.Decenas.Where(u=>u.UserId==user), "Id", "Nombre");
            return View();
        }

        // POST: Cantidades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Numero,Cantidad,Estado,DecenasId")] Cantidades cantidades)
        {
            if (ModelState.IsValid)
            {
               
                db.Cantidades.Add(cantidades);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var user = User.Identity.GetUserId();
            ViewBag.DecenasId = new SelectList(db.Decenas.Where(u=>u.UserId==user), "Id", "Nombre", cantidades.DecenasId);
            return View(cantidades);
        }

        // GET: Cantidades/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cantidades cantidades = await db.Cantidades.FindAsync(id);
            if (cantidades == null)
            {
                return HttpNotFound();
            }
            var user = User.Identity.GetUserId();
            ViewBag.DecenasId = new SelectList(db.Decenas.Where(u => u.UserId == user), "Id", "Nombre", cantidades.DecenasId);
            return View(cantidades);
        }

        // POST: Cantidades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Numero,Cantidad,Estado,DecenasId")] Cantidades cantidades)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cantidades).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var user = User.Identity.GetUserId();
            ViewBag.DecenasId = new SelectList(db.Decenas.Where(u=>u.UserId==user), "Id", "Nombre", cantidades.DecenasId);
            return View(cantidades);
        }

        // GET: Cantidades/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cantidades cantidades = await db.Cantidades.FindAsync(id);
            if (cantidades == null)
            {
                return HttpNotFound();
            }
            return View(cantidades);
        }

        // POST: Cantidades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Cantidades cantidades = await db.Cantidades.FindAsync(id);
            db.Cantidades.Remove(cantidades);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
