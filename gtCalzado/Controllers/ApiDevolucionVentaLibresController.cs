﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using gtCalzado.Models;

namespace gtCalzado.Controllers
{
    public class ApiDevolucionVentaLibresController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ApiDevolucionVentaLibres
        public List<DevolucionesViewModel> GetDevolucionVentaLibre()
        {
            var q = (from d in db.DevolucionVentaLibre
                     join v in db.VentaLibre on d.VentaLibreId equals v.Id
                     select new DevolucionesViewModel
                     {
                         Id=d.Id,
                         Precio=v.Precio,
                         Diferencia=d.Diferencia,
                         Fecha=d.Fecha,
                         Vendedor=v.Vendedor
                     }).ToList();

            return q;
        }

        // GET: api/ApiDevolucionVentaLibres/5
        [ResponseType(typeof(DevolucionVentaLibre))]
        public async Task<IHttpActionResult> GetDevolucionVentaLibre(int id)
        {
            DevolucionVentaLibre devolucionVentaLibre = await db.DevolucionVentaLibre.FindAsync(id);
            if (devolucionVentaLibre == null)
            {
                return NotFound();
            }

            return Ok(devolucionVentaLibre);
        }

        // PUT: api/ApiDevolucionVentaLibres/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDevolucionVentaLibre(int id, DevolucionVentaLibre devolucionVentaLibre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != devolucionVentaLibre.Id)
            {
                return BadRequest();
            }

            db.Entry(devolucionVentaLibre).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DevolucionVentaLibreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiDevolucionVentaLibres
        [ResponseType(typeof(DevolucionVentaLibre))]
        public async Task<IHttpActionResult> PostDevolucionVentaLibre(DevolucionVentaLibre devolucionVentaLibre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            db.DevolucionVentaLibre.Add(devolucionVentaLibre);
            await db.SaveChangesAsync();
            updateStateVenta(devolucionVentaLibre.VentaLibreId);
            return CreatedAtRoute("DefaultApi", new { id = devolucionVentaLibre.Id }, devolucionVentaLibre);
        }

        private  void updateStateVenta(int id)
        {
            VentaLibre ventaLibre =  db.VentaLibre.Find(id);
            if (ventaLibre != null)
            {
                ventaLibre.Estado = "Devolución";
                db.Entry(ventaLibre).State = EntityState.Modified;
                try
                {
                     db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {

                    throw;
                }
            }
        }

        // DELETE: api/ApiDevolucionVentaLibres/5
        [ResponseType(typeof(DevolucionVentaLibre))]
        public async Task<IHttpActionResult> DeleteDevolucionVentaLibre(int id)
        {
            DevolucionVentaLibre devolucionVentaLibre = await db.DevolucionVentaLibre.FindAsync(id);
            if (devolucionVentaLibre == null)
            {
                return NotFound();
            }

            db.DevolucionVentaLibre.Remove(devolucionVentaLibre);
            await db.SaveChangesAsync();

            return Ok(devolucionVentaLibre);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DevolucionVentaLibreExists(int id)
        {
            return db.DevolucionVentaLibre.Count(e => e.Id == id) > 0;
        }
    }
}