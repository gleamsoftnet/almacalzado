﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using gtCalzado.Service;
using Microsoft.AspNet.Identity;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class ZapatosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private SVZapato sVZapato = new SVZapato();
        // GET: Zapatos
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            var zapatos = db.Zapatos.Where(u=>u.UserId==user).Include(z => z.Categoria).Include(z => z.Color).Include(z => z.Proveedor).Include(z => z.Talla);
            return View(await zapatos.ToListAsync());
        }

        // GET: Zapatos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapato zapato = await db.Zapatos.FindAsync(id);
            if (zapato == null)
            {
                return HttpNotFound();
            }
            return View(zapato);
        }

        // GET: Zapatos/Create
        public ActionResult Create()
        {
            var user = User.Identity.GetUserId();
            var ct = new SelectList(db.Categorias.Where(u=>u.UserId==user), "Id", "Nombre");
            ViewBag.CategoriaId = ct;
            ViewBag.CategoriaCount = ct.Count();
            var cl = new SelectList(db.Colores.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.ColorId = cl;
            ViewBag.ColorCount = cl.Count();
            var pd= new SelectList(db.Proveedor.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.ProveedorId = pd;
            ViewBag.ProveedorCount = pd.Count();
            var tl = new SelectList(db.Tallas.Where(u => u.UserId == user), "Id", "Numero");
            ViewBag.TallaId = tl;
            ViewBag.TallaCount = tl.Count();
            return View();
        }

        // POST: Zapatos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Referencia,Nombre,ProveedorId,CategoriaId,TallaId,ColorId,Precio,PrecioVenta,PrecioMinimo,Iva,Cantidad,FechaRegistro,FechaCambio")] Zapato zapato)
        {
            var user = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                int categoriaCode = sVZapato.getCategoryCodeFromId(zapato.CategoriaId);
                int tallaCode = sVZapato.getTallaCodeFromId(zapato.TallaId);
                int colorCode = sVZapato.getColorCodeFromId(zapato.ColorId);
                int proveedorCode = sVZapato.getProveedorCodeFromId(zapato.ProveedorId);
                zapato.Referencia = "p" + proveedorCode + "-ct" + categoriaCode + "-t" + tallaCode + "-c" + colorCode;
                zapato.FechaRegistro = DateTimeOffset.Now;
                zapato.UserId = user;
                db.Zapatos.Add(zapato);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            
            ViewBag.CategoriaId = new SelectList(db.Categorias.Where(u => u.UserId == user), "Id", "Nombre", zapato.CategoriaId);
            ViewBag.ColorId = new SelectList(db.Colores.Where(u => u.UserId == user), "Id", "Nombre", zapato.ColorId);
            ViewBag.ProveedorId = new SelectList(db.Proveedor.Where(u => u.UserId == user), "Id", "Nombre", zapato.ProveedorId);
            ViewBag.TallaId = new SelectList(db.Tallas.Where(u => u.UserId == user), "Id", "Numero", zapato.TallaId);
            return View(zapato);
        }

        // GET: Zapatos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapato zapato = await db.Zapatos.FindAsync(id);
            if (zapato == null)
            {
                return HttpNotFound();
            }
            var user = User.Identity.GetUserId();
            ViewBag.CategoriaId = new SelectList(db.Categorias.Where(u => u.UserId == user), "Id", "Nombre", zapato.CategoriaId);
            ViewBag.ColorId = new SelectList(db.Colores.Where(u => u.UserId == user), "Id", "Nombre", zapato.ColorId);
            ViewBag.ProveedorId = new SelectList(db.Proveedor.Where(u => u.UserId == user), "Id", "Nombre", zapato.ProveedorId);
            ViewBag.TallaId = new SelectList(db.Tallas.Where(u => u.UserId == user), "Id", "Numero", zapato.TallaId);
            return View(zapato);
        }

        // POST: Zapatos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Referencia,Nombre,ProveedorId,CategoriaId,TallaId,ColorId,Precio,PrecioVenta,PrecioMinimo,Iva,Cantidad,FechaRegistro,FechaCambio")] Zapato zapato)
        {
            var user = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                int categoriaCode = sVZapato.getCategoryCodeFromId(zapato.CategoriaId);
                int tallaCode = sVZapato.getTallaCodeFromId(zapato.TallaId);
                int colorCode = sVZapato.getColorCodeFromId(zapato.ColorId);
                int proveedorCode = sVZapato.getProveedorCodeFromId(zapato.ProveedorId);
                zapato.Referencia = "p" + proveedorCode + "-ct" + categoriaCode + "-t" + tallaCode + "-c" + colorCode;
                zapato.FechaCambio = DateTimeOffset.Now;
                zapato.UserId = user;
                db.Entry(zapato).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            
            ViewBag.CategoriaId = new SelectList(db.Categorias.Where(u => u.UserId == user), "Id", "Nombre", zapato.CategoriaId);
            ViewBag.ColorId = new SelectList(db.Colores.Where(u => u.UserId == user), "Id", "Nombre", zapato.ColorId);
            ViewBag.ProveedorId = new SelectList(db.Proveedor.Where(u => u.UserId == user), "Id", "Nombre", zapato.ProveedorId);
            ViewBag.TallaId = new SelectList(db.Tallas.Where(u => u.UserId == user), "Id", "Id", zapato.TallaId);
            return View(zapato);
        }

        // GET: Zapatos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapato zapato = await db.Zapatos.FindAsync(id);
            if (zapato == null)
            {
                return HttpNotFound();
            }
            return View(zapato);
        }

        // POST: Zapatos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Zapato zapato = await db.Zapatos.FindAsync(id);
            db.Zapatos.Remove(zapato);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
