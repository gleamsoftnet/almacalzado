﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;

namespace gtCalzado.Controllers
{
    public class VentaLibresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: VentaLibres
        public async Task<ActionResult> Index()
        {
            return View(await db.VentaLibre.ToListAsync());
        }

        // GET: VentaLibres/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VentaLibre ventaLibre = await db.VentaLibre.FindAsync(id);
            if (ventaLibre == null)
            {
                return HttpNotFound();
            }
            return View(ventaLibre);
        }

        // GET: VentaLibres/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VentaLibres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Factura,Nombre,Cantidad,Precio,SubTotal,Total,Descuento,FormaPago,Vendedor,UsuarioId,Fecha")] VentaLibre ventaLibre)
        {
            if (ModelState.IsValid)
            {
                db.VentaLibre.Add(ventaLibre);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ventaLibre);
        }

        // GET: VentaLibres/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VentaLibre ventaLibre = await db.VentaLibre.FindAsync(id);
            if (ventaLibre == null)
            {
                return HttpNotFound();
            }
            return View(ventaLibre);
        }

        // POST: VentaLibres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Factura,Nombre,Cantidad,Precio,SubTotal,Total,Descuento,FormaPago,Vendedor,UsuarioId,Fecha")] VentaLibre ventaLibre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ventaLibre).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(ventaLibre);
        }

        // GET: VentaLibres/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VentaLibre ventaLibre = await db.VentaLibre.FindAsync(id);
            if (ventaLibre == null)
            {
                return HttpNotFound();
            }
            return View(ventaLibre);
        }

        // POST: VentaLibres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            VentaLibre ventaLibre = await db.VentaLibre.FindAsync(id);
            db.VentaLibre.Remove(ventaLibre);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
