﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gtCalzado.Models;
using Microsoft.AspNet.Identity;

namespace gtCalzado.Controllers
{
    public class RefComprasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RefCompras
        public async Task<ActionResult> Index()
        {
            var user = User.Identity.GetUserId();
            return View(await db.RefCompras.Where(u=>u.UserId==user).ToListAsync());
        }

        // GET: RefCompras/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RefCompra refCompra = await db.RefCompras.FindAsync(id);
            if (refCompra == null)
            {
                return HttpNotFound();
            }
            return View(refCompra);
        }

        // GET: RefCompras/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RefCompras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Nombre,Referencia,Fecha,Estado")] RefCompra refCompra)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                refCompra.UserId = user;
                refCompra.Estado = true;
                refCompra.Fecha = DateTimeOffset.Now;
                refCompra.Referencia = Guid.NewGuid().ToString();
                db.RefCompras.Add(refCompra);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(refCompra);
        }

        // GET: RefCompras/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RefCompra refCompra = await db.RefCompras.FindAsync(id);
            if (refCompra == null)
            {
                return HttpNotFound();
            }
            return View(refCompra);
        }

        // POST: RefCompras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nombre,Referencia,Fecha,Estado")] RefCompra refCompra)
        {
            if (ModelState.IsValid)
            {
                var user = User.Identity.GetUserId();
                refCompra.UserId = user;
                db.Entry(refCompra).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(refCompra);
        }

        // GET: RefCompras/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RefCompra refCompra = await db.RefCompras.FindAsync(id);
            if (refCompra == null)
            {
                return HttpNotFound();
            }
            return View(refCompra);
        }

        // POST: RefCompras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            RefCompra refCompra = await db.RefCompras.FindAsync(id);
            db.RefCompras.Remove(refCompra);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
