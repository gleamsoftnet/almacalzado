﻿using gtCalzado.Models;
using gtCalzado.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static gtCalzado.Models.ManageProductosViewModel;

namespace gtCalzado.Controllers
{
    [Authorize]
    public class ComprarController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private SVCompra sVCompra = new SVCompra();
        private SVZapato sVZapato = new SVZapato();
        private SVTallas sVTallas = new SVTallas();
        // GET: Comprar
        public async Task<ActionResult> Index(int? RefId)
        {
            var user = User.Identity.GetUserId();
            if (RefId == null)
            {
                return View(await db.Compras.Where(u => u.UserId == user).ToListAsync());
            }            
            return View(await db.Compras.Where(u => u.UserId == user && u.RefCompraId==RefId).ToListAsync());
        }
        public async Task<ActionResult> BarCode()
        {
            ViewBag.HostRoute= Request.Url.GetLeftPart(UriPartial.Authority);
            var user = User.Identity.GetUserId();
            return View(await db.BarCode.Where(u => u.UserId == user).ToListAsync());
        }

        public async Task<ActionResult> Nuevo(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapato zapato = await db.Zapatos.FindAsync(id);
            if (zapato == null)
            {
                return HttpNotFound();
            }
            var model = new ComprarViewModel();
            model.ZapatoId = zapato.Id;
            model.Nombre = zapato.Nombre;
            model.ProveedorId = zapato.ProveedorId;
            model.CategoriaId = zapato.CategoriaId;
            model.ColorId = zapato.ColorId;
            model.TallaId = zapato.TallaId;
            model.Stock = zapato.Cantidad;
            model.Precio = zapato.Precio;
            model.PrecioVenta = zapato.PrecioVenta;
            model.UserId = zapato.UserId;

            ViewBag.CategoriaId = new SelectList(db.Categorias, "Id", "Nombre", zapato.CategoriaId);
            ViewBag.ColorId = new SelectList(db.Colores, "Id", "Nombre", zapato.ColorId);
            ViewBag.ProveedorId = new SelectList(db.Proveedor, "Id", "Nombre", zapato.ProveedorId);
            ViewBag.TallaId = new SelectList(db.Tallas, "Id", "Numero", zapato.TallaId);

            ViewBag.RefCompraId = new SelectList(db.RefCompras, "Id", "Nombre");
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Nuevo([Bind(Include = "UserId,ZapatoId,Nombre,ProveedorId,CategoriaId,TallaId,ColorId,Precio,PrecioVenta,Cantidad,RefCompraId")] ComprarViewModel comprar)
        {
            if (ModelState.IsValid)
            {
                Zapato zapato = await db.Zapatos.FindAsync(comprar.ZapatoId);
                if (zapato == null)
                {
                    return HttpNotFound();
                }
                zapato.Cantidad = zapato.Cantidad + comprar.Cantidad;
                zapato.PrecioVenta = comprar.PrecioVenta;
                zapato.FechaCambio = DateTimeOffset.Now;
                db.Entry(zapato).State = EntityState.Modified;
                await db.SaveChangesAsync();
                sVCompra.NuevaCompra(comprar);
                return RedirectToAction("Index", "Zapatos");
            }

            ViewBag.CategoriaId = new SelectList(db.Categorias, "Id", "Nombre", comprar.CategoriaId);
            ViewBag.ColorId = new SelectList(db.Colores, "Id", "Nombre", comprar.ColorId);
            ViewBag.ProveedorId = new SelectList(db.Proveedor, "Id", "Nombre", comprar.ProveedorId);
            ViewBag.TallaId = new SelectList(db.Tallas, "Id", "Numero", comprar.TallaId);
            ViewBag.RefCompraId = new SelectList(db.RefCompras, "Id", "Nombre");
            return View(comprar);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCantidades(int docenasId)
        {
            var query = from obj in db.Cantidades
                        where obj.DecenasId == docenasId
                        select new { Id = obj.Id, Numero = obj.Numero, Cantidad = obj.Cantidad };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Add()
        {
            var user = User.Identity.GetUserId();
            ViewBag.ColorId = new SelectList(db.Colores.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.ProveedorId = new SelectList(db.Proveedor.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.Decenas = new SelectList(db.Decenas.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.RefCompraId = new SelectList(db.RefCompras.Where(u => u.UserId == user), "Id", "Nombre");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ComprarDecenas comprarDecenas)
        {
            var user = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                var zapato = sVZapato.getZapatoDetalle(comprarDecenas.ZapatoId);
                //var zapatos = sVZapato.getZapatosList(zapato.Nombre,comprarDecenas.ProveedorId);
                foreach (var ctadpordocenaId in comprarDecenas.ProductosList)
                {

                    if (zapato.ProveedorId == comprarDecenas.ProveedorId &&
                        zapato.Nombre == zapato.Nombre)
                    {
                        var cantidades = sVZapato.getCantidadesDetalle(ctadpordocenaId);
                        int idTalla = sVZapato.getTallaIdFromNumber(cantidades.Numero, user);
                        if (idTalla > 0)
                        {
                            //el producto existe ya en la base de datos
                            Zapato result = sVZapato.ExisteProducto(user,zapato.Nombre, comprarDecenas.ProveedorId, idTalla, comprarDecenas.ColorId);
                            if (result != null)
                            {
                                var model = new Compra();
                                model.ZapatoId = result.Id;
                                model.ProveedorId = comprarDecenas.ProveedorId;
                                model.CategoriaId = result.CategoriaId;
                                model.ColorId = result.ColorId;
                                model.TallaId = idTalla;
                                model.Cantidad = cantidades.Cantidad;
                                model.Precio = result.Precio;
                                model.PrecioVenta = result.PrecioVenta;
                                model.UserId = user;
                                model.FechaRegistro = DateTimeOffset.Now;
                                model.RefCompraId = comprarDecenas.RefCompraId;
                                sVCompra.NuevaCompraDocenas(model);
                                //actualizar stock
                                sVZapato.UpdateStock(result.Id, cantidades.Cantidad);
                            }
                            else
                            {   //el producto no existe y debe ser creado 
                                //creo el zapado con los números restantes y registro la compra
                                //válido que le producto no exista definitivamente en la base de datos

                                var producto = new Zapato();
                                producto.CategoriaId = zapato.CategoriaId;
                                producto.ColorId = comprarDecenas.ColorId;
                                producto.Nombre = zapato.Nombre;
                                producto.Precio = zapato.Precio;
                                producto.Cantidad = cantidades.Cantidad;
                                producto.PrecioVenta = zapato.PrecioVenta;
                                producto.Iva = 0;
                                producto.TallaId = idTalla;
                                producto.UserId = user;
                                producto.FechaRegistro = DateTimeOffset.Now;
                                producto.ProveedorId = comprarDecenas.ProveedorId;
                                int idNuevoZapato = sVZapato.NuevoProducto(producto);//creo el producto
                                                                                     //lo ingreso como compra
                                var model = new Compra();
                                model.ZapatoId = idNuevoZapato;
                                model.ProveedorId = comprarDecenas.ProveedorId;
                                model.CategoriaId = zapato.CategoriaId;
                                model.ColorId = comprarDecenas.ColorId;
                                model.TallaId = idTalla;
                                model.Cantidad = cantidades.Cantidad;
                                model.Precio = zapato.Precio;
                                model.PrecioVenta = zapato.PrecioVenta;
                                model.UserId = user;
                                model.FechaRegistro = DateTimeOffset.Now;
                                model.RefCompraId = comprarDecenas.RefCompraId;
                                sVCompra.NuevaCompraDocenas(model);

                            }
                            //validar producto  por color- si el producto no existe con ese color, crearlo

                        }else
                        {
                          int nuevaTallaId=  sVTallas.NuevaTalla(user,cantidades.Numero);
                            Zapato result1 = sVZapato.ExisteProducto(user,zapato.Nombre, comprarDecenas.ProveedorId, nuevaTallaId, comprarDecenas.ColorId);
                            if (result1 != null)
                            {
                                var model = new Compra();
                                model.ZapatoId = result1.Id;
                                model.ProveedorId = comprarDecenas.ProveedorId;
                                model.CategoriaId = result1.CategoriaId;
                                model.ColorId = result1.ColorId;
                                model.TallaId = nuevaTallaId;
                                model.Cantidad = cantidades.Cantidad;
                                model.Precio = result1.Precio;
                                model.PrecioVenta = result1.PrecioVenta;
                                model.UserId = user;
                                model.FechaRegistro = DateTimeOffset.Now;
                                model.RefCompraId = comprarDecenas.RefCompraId;
                                sVCompra.NuevaCompraDocenas(model);
                                //actualizar stock
                                sVZapato.UpdateStock(comprarDecenas.ZapatoId, cantidades.Cantidad);
                            }
                            else {
                                var producto = new Zapato();
                                producto.CategoriaId = zapato.CategoriaId;
                                producto.ColorId = comprarDecenas.ColorId;
                                producto.Nombre = zapato.Nombre;
                                producto.Precio = zapato.Precio;
                                producto.Cantidad = cantidades.Cantidad;
                                producto.PrecioVenta = zapato.PrecioVenta;
                                producto.Iva = 0;
                                producto.TallaId = nuevaTallaId;
                                producto.UserId = user;
                                producto.FechaRegistro = DateTimeOffset.Now;
                                producto.ProveedorId = comprarDecenas.ProveedorId;
                                int idNuevoZapato = sVZapato.NuevoProducto(producto);//creo el producto
                                                                                     //lo ingreso como compra
                                var model = new Compra();
                                model.ZapatoId = idNuevoZapato;
                                model.ProveedorId = comprarDecenas.ProveedorId;
                                model.CategoriaId = zapato.CategoriaId;
                                model.ColorId = comprarDecenas.ColorId;
                                model.TallaId = nuevaTallaId;
                                model.Cantidad = cantidades.Cantidad;
                                model.Precio = zapato.Precio;
                                model.PrecioVenta = zapato.PrecioVenta;
                                model.UserId = user;
                                model.FechaRegistro = DateTimeOffset.Now;
                                model.RefCompraId = comprarDecenas.RefCompraId;
                                sVCompra.NuevaCompraDocenas(model);
                            }
                        }

                    }

                }


            }

            ViewBag.ColorId = new SelectList(db.Colores.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.ProveedorId = new SelectList(db.Proveedor.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.Decenas = new SelectList(db.Decenas.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.RefCompraId = new SelectList(db.RefCompras.Where(u => u.UserId == user), "Id", "Nombre");
            ViewBag.Ref = comprarDecenas.RefCompraId;
            return View(comprarDecenas);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadProductos(int proveedorId)
        {
            var query = (from obj in db.Zapatos
                        where obj.ProveedorId == proveedorId                       
                        select new { Id=obj.Id,Nombre=obj.Nombre}).ToList();
            var distinctItems = query.GroupBy(x => x.Nombre).Select(y => y.First()).ToList();
            return Json(distinctItems, JsonRequestBehavior.AllowGet);
        }
    }
}