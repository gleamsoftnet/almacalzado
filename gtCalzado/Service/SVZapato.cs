﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
 

namespace gtCalzado.Service
{
    public class SVZapato
    {

        public int getAsesorIdFromCode(int code)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Asesores.Where(c => c.Codigo == code).SingleOrDefault();
                if (data != null)
                    return data.Id;
                else return 0;
            }
        }
        public  int getCategoryCodeFromId(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Categorias.Where(c => c.Id == Id).SingleOrDefault();
                return data.Codigo;
            }
        }
        public int getColorCodeFromId(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Colores.Where(c => c.Id == Id).SingleOrDefault();
                return data.Codigo;
            }
        }
        public int getTallaCodeFromId(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Tallas.Where(c => c.Id == Id).SingleOrDefault();
                return data.Numero;
            }
        }
        public int getTallaIdFromNumber(int numer,string userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Tallas.Where(c => c.Numero == numer && c.UserId==userId).SingleOrDefault();
                if (data != null)
                    return data.Id;
                else return 0;
            }
        }
        public int getProveedorCodeFromId(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Proveedor.Where(c => c.Id == Id).SingleOrDefault();
                return data.Codigo;
            }
        }
        public List<Zapato> getZapatosList(string nombreProducto,int proveedorId)
        {
            using (var db = new ApplicationDbContext())
            {
               return db.Zapatos.Where(u=> (u.Nombre== nombreProducto) && (u.ProveedorId== proveedorId)).Include(z => z.Categoria).Include(z => z.Color).Include(z => z.Proveedor).Include(z => z.Talla).ToList();

            }
        }
        public Zapato getZapatoDetalle(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Zapatos.Where(z => z.Id == Id).SingleOrDefault();

            }
        }
        public Cantidades getCantidadesDetalle(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Cantidades.Where(z => z.Id == Id).SingleOrDefault();

            }
        }
        public  void UpdateStock(int Id,int cantidadCompra)
        {
            using (var db = new ApplicationDbContext())
            {
                Zapato zapato =   db.Zapatos.Find(Id);
                if (zapato != null)
                {
                    zapato.Cantidad = zapato.Cantidad + cantidadCompra;
                    //zapato.PrecioVenta 
                    zapato.FechaCambio = DateTimeOffset.Now;
                    db.Entry(zapato).State = EntityState.Modified;
                     db.SaveChanges();
                }
                
            }
        }
        public void UpdateStockVenta(int Id, int cantidadCompra)
        {
            using (var db = new ApplicationDbContext())
            {
                Zapato zapato = db.Zapatos.Find(Id);
                if (zapato != null)
                {
                    zapato.Cantidad = zapato.Cantidad - cantidadCompra;
                    //zapato.PrecioVenta 
                    zapato.FechaCambio = DateTimeOffset.Now;
                    db.Entry(zapato).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }
        }
        public int NuevoProducto(Zapato zapato)
        {
            using (var db = new ApplicationDbContext())
            {
                int categoriaCode = getCategoryCodeFromId(zapato.CategoriaId);
                int tallaCode = getTallaCodeFromId(zapato.TallaId);
                int colorCode = getColorCodeFromId(zapato.ColorId);
                int proveedorCode = getProveedorCodeFromId(zapato.ProveedorId);
                zapato.Referencia = "p" + proveedorCode + "-ct" + categoriaCode + "-t" + tallaCode + "-c" + colorCode;
                db.Zapatos.Add(zapato);
                db.SaveChanges();
                return zapato.Id;
            }
        }
        public int NuevoAsesor(int code,string empresaId)
        {
            using (var db = new ApplicationDbContext())
            {
                var asesor = new Asesores();
                asesor.Codigo = code;
                asesor.EmpresaId = empresaId;
                asesor.FechaRegistro = DateTimeOffset.Now;
                asesor.Nombre = "Asesor Cod:" + code;
                db.Asesores.Add(asesor);
                db.SaveChanges();
                return asesor.Id;
            }
            
        }
        public Zapato ExisteProducto(string userId,string nombreProducto,int proveedorId,int tallaId,int colorId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Zapatos.Where(u=>u.UserId==userId && u.Nombre==nombreProducto && u.ProveedorId==proveedorId
                &&u.TallaId==tallaId && u.ColorId==colorId).SingleOrDefault(); 
            }
        }
    }
}