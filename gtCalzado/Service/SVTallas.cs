﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Service
{
    public class SVTallas
    {
        public  int NuevaTalla(string usuarioId,int numero)
        {
            using (var db = new ApplicationDbContext())
            {
                //var query = db.Tallas.Where(u=>u.UserId==usuarioId).OrderByDescending(u=>u.Id).Take(1).SingleOrDefault();
                var talla = new Talla();
                int newCode = Convert.ToInt32(string.Format("{0}{1}", 0, numero));
                talla.UserId = usuarioId;
                talla.Numero = numero;
                talla.Codigo = newCode;
                db.Tallas.Add(talla);
                db.SaveChanges();
                return talla.Id;

            }
            
        }
    }
}