﻿using gtCalzado.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace gtCalzado.Service
{
    public class SVUsers
    {
        public void ActiveCount(string userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(u => u.Id == userId).SingleOrDefault();
                if (user != null)
                {
                    //if (user.Conectado)
                    //{
                    //    user.Conectado = false;
                    //}
                    //else
                    //{
                    //    user.Conectado = true;
                    //}

                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                }
            }

        }
        public ApplicationUser GetUserData(string username)
        {
            using (var db = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                ApplicationUser currentUser = UserManager.FindByName(username);
                return currentUser;
            }
        }
        public void UpdateImageProfile(string userId, string urlPerfil)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(u => u.Id == userId).SingleOrDefault();
                if (user != null)
                {
                    //user.Avatar = urlPerfil;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                }
            }


        }
    }
}