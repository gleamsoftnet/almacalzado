﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace gtCalzado.Service
{
    public class SVAuthentication
    {
        public string GetUsersFromId(string username)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = (from u in db.Users
                            where u.UserName.Contains(username ?? string.Empty)
                            select new
                            {
                                u.UserName,

                            }).FirstOrDefault();
                if (data == null)
                    return "";
                return data.UserName;
            }
        }
        public UserInfoViewModel GetDataUserId(string username)
        {
            using (var db = new ApplicationDbContext())
            {

                return (from u in db.Users
                        from ur in u.Roles
                        join r in db.Roles on ur.RoleId equals r.Id
                        where u.UserName == username
                        select new UserInfoViewModel
                        {
                            Id = u.Id,
                            Correo = u.Email,
                            Usuario = u.UserName,
                            Estado = u.Estado,
                            Rol = r.Name,                            
                            Nombres = u.Nombre,                            
                            FechaRegistro = u.CreatedAt,                           

                        }).FirstOrDefault();

            }
        }
        public void isSession(string userId, bool state)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(u => u.UserName == userId).SingleOrDefault();
                if (user != null)
                {
                    //user.Conectado = state;
                    //user.CurrentConnect = user.CurrentConnect + 1;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                }
            }

        }
    }
}
