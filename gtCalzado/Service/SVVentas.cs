﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Service
{
    public class SVVentas
    {
        public void NuevaVenta(Ventas ventas)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Ventas.Add(ventas);
                db.SaveChanges();
            }
        }
    }
}