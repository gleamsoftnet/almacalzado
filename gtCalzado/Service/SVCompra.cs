﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static gtCalzado.Models.ManageProductosViewModel;

namespace gtCalzado.Service
{
    public class SVCompra
    {
        public  void NuevaCompra(ComprarViewModel compras)
        {
            using (var db = new ApplicationDbContext())
            {
                var nuevaCompra = new Compra();
                    nuevaCompra.ZapatoId = compras.ZapatoId;
                    nuevaCompra.ProveedorId = compras.ProveedorId;
                    nuevaCompra.CategoriaId = compras.CategoriaId;
                    nuevaCompra.ColorId = compras.ColorId;
                    nuevaCompra.TallaId = compras.TallaId;
                    nuevaCompra.Precio = compras.Precio;
                    nuevaCompra.PrecioVenta = compras.PrecioVenta;
                    nuevaCompra.Cantidad = compras.Cantidad;
                    nuevaCompra.FechaRegistro = DateTimeOffset.Now;
                    nuevaCompra.UserId = compras.UserId;
                    nuevaCompra.RefCompraId = compras.RefCompraId;
                    db.Compras.Add(nuevaCompra);
                 db.SaveChanges();
            }
        }
        public void NuevaCompraDocenas(Compra compras)
        {
            using (var db = new ApplicationDbContext())
            {    
                db.Compras.Add(compras);
                db.SaveChanges();
            }
        }
        public void NuevaRutaBarCode(BarCode barCode)
        {
            using (var db = new ApplicationDbContext())
            {
                db.BarCode.Add(barCode);
                db.SaveChanges();
            }
        }
        public List<CodigosBarras> GetCodigosBarras(int RefCompraId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from c in db.Compras
                        join z in db.Zapatos on c.ZapatoId equals z.Id
                        where c.RefCompraId == RefCompraId
                        select new CodigosBarras
                        {
                            Referencia = z.Referencia,
                            Id = z.Id,
                            Nombre = z.Nombre,
                            Precio = z.Precio,
                            Proveedor = z.Proveedor.Nombre,
                            ReferenciaId = c.RefCompraId,
                            Cantidad=c.Cantidad
                        }).ToList();

            }
        }

    }
}