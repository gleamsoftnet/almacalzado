﻿using gtCalzado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Service
{
    public class SVUsuriosRoles
    {
        public List<ManagerUserViewModel> GetUsersDDL(string roleName, string empresaId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from u in db.Users
                        from ur in u.Roles
                        join r in db.Roles on ur.RoleId equals r.Id
                        where r.Name == roleName && u.EmpresaId == empresaId

                        select new ManagerUserViewModel
                        {
                            Id = u.Id,
                            Usuario=u.UserName,
                            Fecha=u.CreatedAt,
                            Rol = r.Name,
                            Correo = u.Email,
                            Nombres = u.Nombre
                        }).ToList();

            }
        }

        public List<ManagerUserViewModel> GetUsersEmpresa(string empresaId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from u in db.Users
                        from ur in u.Roles
                        join r in db.Roles on ur.RoleId equals r.Id
                        where u.EmpresaId == empresaId

                        select new ManagerUserViewModel
                        {
                            Id = u.Id,
                            Fecha = u.CreatedAt,
                            Rol = r.Name,
                            Correo = u.Email,
                            Nombres = u.Nombre,
                            Usuario = u.UserName,
                            Estado = u.Estado,

                        }).ToList();

            }
        }
    }
}