﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class UserInfoViewModel
    {
        public string Id { get; set; }
        public string Usuario { get; set; }
        public string Correo { get; set; }        
        public DateTimeOffset FechaRegistro { get; set; }
        public string Rol { get; set; }
        public bool Estado { get; set; }
        public string Nombres { get; set; }
    
    }
}