﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class ZapatoViewModel
    {
        public int Id { get; set; }
        public string Referencia { get; set; }
        public string Nombre { get; set; }
        public string Proveedor { get; set; }
        public int Color { get; set; }
        public int Talla { get; set; }
        public int Categoria { get; set; }
        public decimal Precio { get; set; }
        public decimal PrecioVenta { get; set; }
        public int Iva { get; set; }    
        public int Cantidad { get; set; }        
        public DateTimeOffset FechaRegistro { get; set; }
    }
}