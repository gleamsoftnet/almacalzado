﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Asesores")]
    public class Asesores
    {
        public int Id { get; set; }
        [Display(Name = "Código")]
        public int Codigo { get; set; }
        [StringLength(100)]
        public string Nombre { get; set; }
        [Display(Name = "Fecha")]
        public DateTimeOffset FechaRegistro { get; set; }
        [StringLength(250)]
        public  string EmpresaId { get; set; }
        
    }
}