﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class BarCode
    {
        public int Id { get; set; }
        public string Proveedor { get; set; }
        public string Ruta { get; set; }
        public int Referencia { get; set; }
        public DateTimeOffset Fecha { get; set; }
        public string UserId { get; set; }
    }
}