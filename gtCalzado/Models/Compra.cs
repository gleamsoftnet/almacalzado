﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Compra")]
    public class Compra
    {
        public int Id { get; set; }       
       
        public int ZapatoId { get; set; }      

        [Display(Name = "Proveedor")]
        [ForeignKey("Proveedor")]
        public int ProveedorId { get; set; }
        public virtual Proveedor Proveedor { get; set; }
        [Display(Name = "Categoría")]
        [ForeignKey("Categoria")]
        public int CategoriaId { get; set; }
        public virtual Categoria Categoria { get; set; }
        [Display(Name = "Talla")]
        [ForeignKey("Talla")]
        public int TallaId { get; set; }
        public virtual Talla Talla { get; set; }
         
        [ForeignKey("RefCompra")]
        public int RefCompraId { get; set; }
        public virtual RefCompra RefCompra { get; set; }
        [Display(Name = "Color")]
        [ForeignKey("Color")]
        public int ColorId { get; set; }
        public virtual Color Color { get; set; }
        [Required]
        public decimal Precio { get; set; }
        [Display(Name = "Precio venta")]
        [Required]
        public decimal PrecioVenta { get; set; }
        public int Iva { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Display(Name = "Fecha registro")]
        public DateTimeOffset FechaRegistro { get; set; }
        [Display(Name = "Fecha último cambio")]
        public DateTimeOffset FechaCambio { get; set; }
        [StringLength(120)]
        public string UserId { get; set; }
    }
}