﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Ventas")]
    public class Ventas
    {
        public int Id { get; set; }
        public bool Estado { get; set; }
        public Guid Referencia { get; set; }
        public string MetodoPago { get; set; }
        [StringLength(120)]
        public string VendedorId { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public int Cantidad { get; set; }
        public decimal Descuento { get; set; }
        [ForeignKey("Clientes")]
        public int ClienteId { get; set; }
        public virtual Clientes Clientes { get; set; }
        [ForeignKey("Zapato")]
        public int ZapatoId { get; set; }
        public virtual Zapato Zapato { get; set; }
        [ForeignKey("Asesores")]
        public int AsesorId { get; set; }
        public virtual Asesores Asesores { get; set; }
    }
}