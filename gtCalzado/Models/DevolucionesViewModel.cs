﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class DevolucionesViewModel
    {
        public int Id { get; set; }
        public decimal Precio { get; set; }
        public decimal Diferencia { get; set; }
        public int Vendedor { get; set; }
        public DateTimeOffset Fecha { get; set; }
    }
}