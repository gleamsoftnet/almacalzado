﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("RefCompras")]
    public class RefCompra
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Nombre { get; set; }
        [StringLength(50)]
        public string Referencia { get; set; }
        public DateTimeOffset Fecha { get; set; }
        public bool Estado { get; set; }
        [StringLength(250)]
        public string UserId { get; set; }
        public virtual ICollection<Compra> Compra { get; set; }
    }
}