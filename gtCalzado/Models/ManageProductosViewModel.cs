﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class ManageProductosViewModel
    {
        public class ComprarViewModel
        {

            public int ZapatoId { get; set; }
            public string Nombre { get; set; }
           
            public int ProveedorId { get; set; }
           
            public int CategoriaId { get; set; }
           
            public int ColorId { get; set; }
           
            public int TallaId { get; set; }
           
            public int Stock { get; set; }
            public decimal Precio { get; set; }
            [Display(Name ="Precio venta")]
            public decimal PrecioVenta { get; set; }
            [Display(Name = "Nueva entrada")]
            [Required]
            public int Cantidad { get; set; }
            public int RefCompraId { get; set; }
            public string UserId { get; set; }
        }
        public class ComprarDecenas
        {
            [Required]
            public int ColorId { get; set; }
            [Required]
            [Display(Name = "Proveedor")]
            public int ProveedorId { get; set; }
            [Required]
            [Display(Name = "Docenas")]
            public int Decenas { get; set; }
            [Required]
            [Display(Name = "Referencia")]
            public int RefCompraId { get; set; }
            [Required]
            [Display(Name ="Producto")]
            public int ZapatoId { get; set; }
            [Required]
            public List<int> ProductosList { get; set; }
        }
        public class CodigosBarras
        {
            public int Id { get; set; }
            public string Referencia { get; set; }
            public decimal Precio { get; set; }
            public string Nombre { get; set; }
            public string Proveedor { get; set; }
            public int ReferenciaId { get; set; }
            public int Cantidad { get; set; }
        }
    }
}