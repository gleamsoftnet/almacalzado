﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class VentaLibre
    {
        public int Id { get; set; }
        public string Factura { get; set; }
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public decimal Descuento { get; set; }
        public string FormaPago { get; set; }
        public int Vendedor { get; set; }
        public string UsuarioId { get; set; }
        public DateTimeOffset Fecha { get; set; }
        public string Estado { get; set; }
    }
}