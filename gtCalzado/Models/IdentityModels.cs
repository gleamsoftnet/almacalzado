﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace gtCalzado.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        [StringLength(120)]
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        [StringLength(120)]
        public string Direccion { get; set; }
        public string EmpresaId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }
     
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("AlmacalzadoConnection", throwIfV1Schema: false)
        {
        }
        public DbSet<Proveedor> Proveedor { get; set; }
        public DbSet<Talla> Tallas { get; set; }
        public DbSet<Color> Colores { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Zapato> Zapatos{ get; set; }
        public DbSet<Compra> Compras { get; set; }
        public DbSet<Sucursales> Sucursales { get; set; }
        public DbSet<RefCompra> RefCompras { get; set; }
        public DbSet<Decenas> Decenas { get; set; }
        public DbSet<Cantidades> Cantidades { get; set; }
        public DbSet<BarCode> BarCode { get; set; }
        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<Ventas> Ventas { get; set; }
        public DbSet<Asesores> Asesores { get; set; }
        public DbSet<VentaLibre> VentaLibre { get; set; }
        public DbSet<DevolucionVentaLibre> DevolucionVentaLibre { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        

    }
}