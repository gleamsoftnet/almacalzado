﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class DevolucionVentaLibre
    {
        public int Id { get; set; }
        public decimal Diferencia { get; set; }
        public string UsuarioId { get; set; }
        public DateTimeOffset Fecha { get; set; }
        [ForeignKey("VentaLibre")]
        public int VentaLibreId { get; set; }
        public virtual VentaLibre VentaLibre { get; set; }
    }
}