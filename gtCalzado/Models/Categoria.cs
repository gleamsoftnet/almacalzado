﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Categoria")]
    public class Categoria
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="Código")]
        public int Codigo { get; set; }
        [StringLength(120)]
        [Required]
        [Display(Name = "Nombre Categoría")]
        public string Nombre { get; set; }
        public bool Estado { get; set; }        
        public string Avatar { get; set; }
        [StringLength(120)]
        public string UserId { get; set; }
        public virtual ICollection<Zapato> Zapato { get; set; }
    }
}