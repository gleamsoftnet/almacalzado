﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    public class VenderViewModel
    {
        public decimal Descuento { get; set; }
        public string MetodoPago { get; set; }
        public int AsesorId { get; set; }
        public int ClienteId { get; set; }
        public List<ProductoVenta> ProductoVenta { get; set; }
    }
    public class ProductoVenta {
        public int ProductoId { get; set; }
    }
}