﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Talla")]
    public class Talla
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Código")]
        public int Codigo { get; set; }
        [Required]
        [Display(Name ="Talla número")]
        public int Numero { get; set; }
        [StringLength(120)]
        public string UserId { get; set; }
        public virtual ICollection<Zapato> Zapato { get; set; }
    }
}