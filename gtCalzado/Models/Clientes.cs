﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Clientes")]
    public class Clientes
    {
        public int Id { get; set; }
        [Display(Name ="Cédula")]
        [Required]
        public int Cedula { get; set; }
        public string Nombre { get; set; }
        [Display(Name ="Teléfono")]
        public int Telefono { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public string Autor { get; set; }

    }
}