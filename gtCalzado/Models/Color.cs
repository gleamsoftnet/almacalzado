﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Color")]
    public class Color
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Código")]
        public int Codigo { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name ="Nombre color")]
        public string Nombre { get; set; }
        [StringLength(120)]
        public string UserId { get; set; }
        public virtual ICollection<Zapato> Zapato { get; set; }
    }
}