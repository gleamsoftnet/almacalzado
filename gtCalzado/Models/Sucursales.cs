﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Sucursales")]
    public class Sucursales
    {
        public int Id { get; set; }
        [StringLength(100)]
        [Required]
        [Display(Name = "Sucursal")]
        public string Nombre { get; set; }
        [StringLength(100)]
        [Required]
        [Display(Name ="Dirección")]
        public string Direccion { get; set; }
        public bool Estado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        [StringLength(200)]         
        public string UserId { get; set; }
        public virtual ICollection<Proveedor> Proveedor { get; set; }
    }
}