﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Proveedor")]
    public class Proveedor
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Código")]
        public int Codigo { get; set; }
        [Display(Name ="Cédula")]
        [StringLength(12)]
        public string Cedula { get; set; }
        [StringLength(120)]
        [Display(Name = "Proveedor")]
        public string Nombre { get; set; }
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }
        public bool Estado { get; set; }
        [StringLength(120)]
        public string UserId { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        [ForeignKey("Sucursales")]
        public int SucursalId { get; set; }
        public virtual Sucursales Sucursales { get; set; }
        public virtual ICollection<Zapato> Zapato { get; set; }
    }
}