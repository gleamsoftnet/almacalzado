﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Cantidades")]
    public class Cantidades
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="Número")]
        public int Numero { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public bool Estado { get; set; }
        [Display(Name = "Decenas")]
        [ForeignKey("Decenas")]
        public int DecenasId { get; set; }
        public virtual Decenas Decenas { get; set; }
    }
}