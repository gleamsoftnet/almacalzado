﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gtCalzado.Models
{
    [Table("Decenas")]
    public class Decenas
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Nombre { get; set; }
        [StringLength(250)]
        public string UserId { get; set; }
        public virtual ICollection<Cantidades> Cantidades { get; set; }
    }
}